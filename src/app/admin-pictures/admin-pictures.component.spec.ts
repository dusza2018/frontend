import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPicturesComponent } from './admin-pictures.component';

describe('AdminPicturesComponent', () => {
  let component: AdminPicturesComponent;
  let fixture: ComponentFixture<AdminPicturesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPicturesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPicturesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
