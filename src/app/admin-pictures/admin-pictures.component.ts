import {Component, OnInit} from '@angular/core';
import * as moment from 'moment';
import {ConfirmationService, MessageService, SelectItem} from 'primeng/api';
import {IFilterObj} from '../pictures/pictures.component';
import {IRevision} from '../scoring/scoring.component';
import {BaseService} from '../shared/services/BaseService';
import {UserStorage} from '../shared/services/UserStorage';

@Component({
    selector: 'app-admin-pictures',
    templateUrl: './admin-pictures.component.html',
    styleUrls: ['./admin-pictures.component.css']
})
export class AdminPicturesComponent implements OnInit {

    filters: IFilterObj[] = <any>[
        {
            withRevisions: 1,

        },
        {
            withRevisions: 1,
            revisionOrder: 1,
        },
    ];

    private arrIndex = 0;

    public optionsForSelect: SelectItem[] = [
        {label: 'Kategóriánként az értékelések szerint', value: 0},
        {label: 'Abszolút sorrend', value: 1}];

    public filterMode = 1;
    public pictures: any[] = [];
    public revision: IRevision = <any>{mentor_id: UserStorage.getUser().id, picture_id: 3};
    public editorVisible: boolean = false;
    public picturePath: string = '';

    public actualImg = {};
    public modalVisible: boolean = false;

    constructor(
        private confirmationService: ConfirmationService,
        private msgs: MessageService,
        private base: BaseService
    ) {
    }

    ngOnInit() {
        this.fetchImages();
    }

    public fetchImages() {
        this.base.post('pictures/filterimages', this.filters[this.filterMode]).subscribe(
            (resp) => {
                this.pictures = resp;
            }, () => {
                this.msgs.add({
                    severity: 'error',
                    summary: 'Nem sikerült a betöltés!'
                });
            }
        );
    }


    public clickOnImg(img) {
        this.actualImg = img;
        this.modalVisible = true;
    }


}
