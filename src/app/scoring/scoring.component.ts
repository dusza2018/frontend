import {Component, OnInit} from '@angular/core';
import * as moment from 'moment';
import {ConfirmationService, MessageService, SelectItem} from 'primeng/api';
import {IFilterObj} from '../pictures/pictures.component';
import {BaseService} from '../shared/services/BaseService';
import {UserStorage} from '../shared/services/UserStorage';

@Component({
    selector: 'app-scoring',
    templateUrl: './scoring.component.html',
    styleUrls: ['./scoring.component.css']
})
export class ScoringComponent implements OnInit {

    filters: IFilterObj[] = <any>[
        {
            withRevisions: 1,
            revisionsOfMentorId: UserStorage.getUser().id
        },
        {
            withRevisions: 1,
            revisionsOfMentorId: UserStorage.getUser().id,
            orderBy: 'reviewed',
            orderDirection: 'asc'
        },
        {
            withRevisions: 1,
            revisionsOfMentorId: UserStorage.getUser().id,
            orderBy: 'reviewed',
            orderDirection: 'desc'
        },
        {
            withRevisions: 1,
            revisionsOfMentorId: UserStorage.getUser().id,
            orderBy: 'liked',
            orderDirection: 'desc'
        },
        {
            withRevisions: 1,
            revisionsOfMentorId: UserStorage.getUser().id,
            orderBy: 'visited',
            orderDirection: 'desc'
        },
        {
            withRevisions: 1,
            revisionsOfMentorId: UserStorage.getUser().id,
            orderBy: 'visited',
            orderDirection: 'asc'
        }
    ];

    private arrIndex = 0;

    public optionsForSelect: SelectItem[] = [
        {label: 'Véletlenszerű sorrend', value: 0},
        {label: 'Legkevesebbszer értékelt', value: 1},
        {label: 'Legtöbbször értékelt', value: 2},
        {label: 'Legtöbb felhasználói értékelés (like)', value: 3},
        {label: 'Legnépszerűbb (legtöbbször megtekintett)', value: 4},
        {label: 'Legkevesebb megtekintés', value: 5}
    ];

    public filterMode = 1;
    public pictures: any[] = [];
    public revision: IRevision = <any>{mentor_id: UserStorage.getUser().id, picture_id: 3};
    public editorVisible: boolean = false;
    public picturePath: string = '';

    constructor(
        private confirmationService: ConfirmationService,
        private msgs: MessageService,
        private base: BaseService
    ) {
    }

    ngOnInit() {
        this.fetchImages();
    }

    public fetchImages() {
        this.base.post('pictures/filterimages', this.filters[this.filterMode]).subscribe(
            (resp) => {
                this.pictures = resp;
                this.pictures = this.pictures.map(e => {
                    if (!!e.revisions.length) {
                        e.revision = e.revisions[0];
                    }
                    else {
                        e.revision = {value: 0};
                    }
                    return e;
                });
            }, () => {
                this.msgs.add({
                    severity: 'error',
                    summary: 'Nem sikerült a betöltés!'
                });
            }
        );
    }

    confirm(picture) {
        this.confirmationService.confirm({
            message: 'Biztos benne hogy ki szeretné tiltani?',
            accept: () => {
                picture.refused_at = moment().format('YYYY.MM.DD HH:mm:ss');
                this.base.update('pictures', picture.id, picture).subscribe(
                    () => {
                        this.pictures = this.pictures.filter(e => e.id !== picture.id);
                        this.msgs.add({
                            severity: 'success',
                            summary: 'Sikeres mentés!'
                        });
                    },
                    () => {
                        this.msgs.add({
                            severity: 'error',
                            summary: 'Nem sikerült a kizárás!'
                        });
                    }
                )
            }
        });
    }

    save(revision: IRevision, picId: number, arrayIndex: number) {
        if (!revision.value) {
            return;
        }
        revision.mentor_id = UserStorage.getUser().id;
        revision.picture_id = picId;

        this.base.post('revisions', revision).subscribe((resp) => {
            this.pictures[arrayIndex].revision = resp;
            this.msgs.add({
                severity: 'success',
                summary: 'Sikeres mentés!'
            });
        }, () => {
            this.msgs.add({
                severity: 'error',
                summary: 'Nem sikerült elmenteni az értékelést!'
            });
        });

        this.base.get('pictures/imageReviewed/' + revision.picture_id).subscribe();
        this.pictures[arrayIndex].reviewed++;
    }

    updateRevision(rev: IRevision) {
        if (!rev.value) {
            return;
        }
        this.base.update('revisions', rev.id, rev).subscribe(
            () => {
                this.msgs.add({
                    severity: 'success',
                    summary: 'Sikeres mentés!'
                });
            },
            () => {
                this.msgs.add({
                    severity: 'error',
                    summary: 'Nem sikerült elmenteni az értékelést!'
                });
            }
        );
    }

    startEditor(filepath: string, arrayIndex: number) {
        this.arrIndex = arrayIndex;
        this.picturePath = filepath;
        setTimeout(() => {
            this.editorVisible = true;
        }, 100);

    }

    partUpdated(event) {
        if (!!event.file_path) {
            this.pictures[this.arrIndex].revision.file_path = event.file_path;
        }
    }

}


export interface IRevision {
    id?: number,
    mentor_id: number,
    picture_id: number,
    description: string,
    value: number
}
