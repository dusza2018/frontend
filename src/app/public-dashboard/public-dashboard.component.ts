import {Component, OnInit} from '@angular/core';
import {MessageService} from 'primeng/primeng';
import {IFilterObj} from '../pictures/pictures.component';
import {Category} from '../settings/settings.component';
import {BaseService} from '../shared/services/BaseService';
import {CategoryStorage} from '../shared/services/CategoryStorage';

@Component({
    selector: 'app-public-dashboard',
    templateUrl: './public-dashboard.component.html',
    styleUrls: ['./public-dashboard.component.css']
})
export class PublicDashboardComponent implements OnInit {
    categories: Category[] = [];
    public filterObj: IFilterObj = {};
    public imagesByCategory: any = {};
    display: boolean = false;
    public actualImg: any = {};

    constructor(private base: BaseService, private msgs: MessageService) {
    }

    ngOnInit() {
        this.categories = CategoryStorage.getCats();
        this.filterObj = <any>{
            groupBy: 'category',
            take: 3
        };
        this.fetchMyImages();
    }

    private fetchMyImages() {
        this.base.post('pictures/filterimages', this.filterObj).subscribe(
            (resp) => {
                this.imagesByCategory = resp;
            },
            () => {
                this.msgs.add({
                    severity: 'error',
                    summary: 'Nem sikerükt a fotók betöltése!'
                });
            }
        );
    }

    show(pic) {

        this.display = true;
        this.actualImg = pic;
    }


}
