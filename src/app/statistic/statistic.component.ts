import {Component, OnInit} from '@angular/core';
import {BaseService} from '../shared/services/BaseService';

@Component({
    selector: 'app-statistic',
    templateUrl: './statistic.component.html',
    styleUrls: ['./statistic.component.css']
})
export class StatisticComponent implements OnInit {
    data: any;
    names:string[]=[];
    revisions:number[]=[];
    colors:string[]=[];

    public datas = [];

    constructor(private base: BaseService) {}

    ngOnInit() {
        this.base.get('getmentorstats').subscribe(
            (resp) => {
            this.datas = resp;
            this.names=this.datas.map(e => e.user.name);
            this.revisions=this.datas.map(e => e.total);
            this.names.forEach((e,i)=>this.colors[i]=`rgb(${Math.floor(Math.random()*255)},${Math.floor(Math.random()*255)},${Math.floor(Math.random()*255)})`);
            this.data = {
                labels: this.names,
                datasets: [
                    {
                        data: this.revisions,
                        backgroundColor: this.colors
                    }]    
                };
            },
            () => {});
           
    }
  


}

