import {IUser} from './auth.service';

export class UserStorage {

    public static key = 'user';

    public static getUser(): IUser {
       let user =  localStorage.getItem(UserStorage.key);
       if (!user) {
           return null;
       }
       return JSON.parse(user);
    }

    public static setUser(user) {
        localStorage.setItem(UserStorage.key, JSON.stringify(user));
    }

}