import {Category} from '../../settings/settings.component';

export class ImageVisitsStorage {

    public static key = 'image_visits';
    public static likeKey = 'image_liked';

    public static getVisitedIDs(): number[] {
        let ids = localStorage.getItem(ImageVisitsStorage.key);
        if (!ids) {
            return [];
        }
        return JSON.parse(ids);
    }

    public static addId(id) {
        let ids = ImageVisitsStorage.getVisitedIDs();
        ids.push(id);
        localStorage.setItem(ImageVisitsStorage.key, JSON.stringify(ids));
    }

    public static getLikedIDs(): number[] {
        let ids = localStorage.getItem(ImageVisitsStorage.likeKey);
        if (!ids) {
            return [];
        }
        return JSON.parse(ids);
    }

    public static addLikeId(id) {
        let ids = ImageVisitsStorage.getLikedIDs();
        ids.push(id);
        localStorage.setItem(ImageVisitsStorage.likeKey, JSON.stringify(ids));
    }


}