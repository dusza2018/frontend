export class FileNumStorage {

    public static key = 'uploaded_files';

    public static setFileNumsByCategory(nums: any) {
        localStorage.setItem(FileNumStorage.key, JSON.stringify(nums));
    }

    public static getFileNumsByCategory() {
        let nums = localStorage.getItem(FileNumStorage.key);
        if (!nums) {
            return null;
        }
        return JSON.parse(nums);
    }

    public static getSum() {
        let datas = FileNumStorage.getFileNumsByCategory();
        if (!datas) {
            return null;
        }
        let sum = 0;
        for (let key in datas) {
            if (datas.hasOwnProperty(key)) {
                sum += datas[key];
            }
        }
        return sum;
    }

}