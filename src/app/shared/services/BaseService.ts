import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';

@Injectable()
export class BaseService {

    public static BASE_URL = environment.backend + '/api/';

    constructor(public http: HttpClient) {
    }

    public get(baseUrl: string): Observable<any> {
        return this.http.get(BaseService.BASE_URL+baseUrl);
    }

    public post(baseUrl: string, data: any): Observable<any> {
        return this.http.post(BaseService.BASE_URL+baseUrl, data);
    }

    public update(baseUrl: string, id: number, data: any): Observable<any> {
        return this.http.put(BaseService.BASE_URL+baseUrl+'/'+id, data);
    }

    public delete(baseUrl: string, id: number): Observable<any> {
      return this.http.delete(BaseService.BASE_URL+baseUrl+'/'+id);
    }
}