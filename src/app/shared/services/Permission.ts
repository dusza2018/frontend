export class Permission {
    public static admin = 3;
    public static mentor = 2;
    public static artist = 1;
}