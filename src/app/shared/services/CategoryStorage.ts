import {Category} from '../../settings/settings.component';

export class CategoryStorage {

    public static key = 'categ';

    public static getCats(): Category[]  {
        let cat =  localStorage.getItem(CategoryStorage.key);
        if (!cat) {
            return null;
        }
        return JSON.parse(cat);
    }

    public static setCats(cat) {
        localStorage.setItem(CategoryStorage.key, JSON.stringify(cat));
    }

}