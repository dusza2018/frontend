import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {AuthService} from './auth.service';
import {Permission} from './Permission';
import {UserStorage} from './UserStorage';

@Injectable()
export class MentorGuard implements CanActivate {

    constructor(
        private auth: AuthService,
        private router: Router
    ) {
    }

    canActivate() {
        if (this.auth.isLoggedIn() &&
            (UserStorage.getUser().permission === Permission.mentor || UserStorage.getUser().permission === Permission.admin)) {
            return true;
        } else {
            this.router.navigate(['/bejelentkezes']);
        }
        return false;
    }
}
