import { Pipe, PipeTransform } from '@angular/core';
import {environment} from '../../../environments/environment';

@Pipe({
  name: 'imageLink'
})
export class ImageLinkPipe implements PipeTransform {

  transform(value: string): any {
    return environment.backend+environment.imageLink+value;
  }

}
