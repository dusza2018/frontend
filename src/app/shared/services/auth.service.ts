import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {JwtHelperService} from '@auth0/angular-jwt';
import {environment} from '../../../environments/environment';
import {UserStorage} from './UserStorage';


export interface ICredentials {
    username: string;
    password: string;
}

export interface IUser {
    name: string,
    permission: number,
    logo_file_path?: string
    username: string,
    id: number,
}

@Injectable()
export class AuthService implements OnInit {
    public static TOKEN_NAME = 'access_token';
    public static LOGIN_ROUTE = '/api/auth/login';
    private timeout;


    constructor(
        private http: HttpClient,
        private jwtHelper: JwtHelperService,
        private router: Router
    ) {
    }

    public ngOnInit() {
    }

    public isLoggedIn() {
        return (!!this.jwtHelper.tokenGetter() && !this.jwtHelper.isTokenExpired());
    }

    public logOut(): void {
        localStorage.setItem(AuthService.TOKEN_NAME, void(0));
        localStorage.clear();
        sessionStorage.clear();
        this.goToLoginPage();
    }

    public goToLoginPage(): void {
        this.router.navigate(['/bejelentkezes']);
    }

    public doLogin(credentials: ICredentials): Promise<void> {
        let headers: HttpHeaders = new HttpHeaders({'Content-Type': 'application/json'});
        return this.http.post(environment.backend + AuthService.LOGIN_ROUTE, credentials, {headers: headers})
            .toPromise()
            .then(
                (resp: ITokenResponse) => {
                    UserStorage.setUser(resp.user);
                    this.setNewTokenAndRenew(resp);
                }
            )
    }

    public startRenewing(token: string, interval: number) {
        if (!this.isLoggedIn()) {
            return;
        }
        if (this.timeout) {
            clearTimeout(this.timeout);
        }
        this.timeout = setTimeout(() => {
            this.http.get(environment.backend + '/api/auth/refresh').subscribe(
                (resp: ITokenResponse) => {
                    this.setNewTokenAndRenew(resp);
                },
                () => {
                    this.goToLoginPage();
                }
            );
        }, interval);
    }

    public setNewTokenAndRenew(resp: ITokenResponse) {
        localStorage.setItem(AuthService.TOKEN_NAME, resp.token);
        //A (-60s) azert, hogy semmikeppen se jarjon le a token mielott ujra frissitene..
        this.startRenewing(resp.token, (resp.expires_in * 1000) - 60000);
    }

}

interface ITokenResponse {
    user: IUser
    token: string,
    expires_in: number
}

