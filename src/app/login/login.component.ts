import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {MessageService} from 'primeng/api';
import {AppComponent} from '../app.component';
import {AuthService} from '../shared/services/auth.service';
import {Permission} from '../shared/services/Permission';
import {UserStorage} from '../shared/services/UserStorage';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    credentials: ICredentials = {username: '', password: ''};
    private error: boolean = false;
    private processing: boolean = false;

    constructor(private auth: AuthService,
                private router: Router,
                private msgs: MessageService,
                private appComp: AppComponent) {
    }

    ngOnInit() {
    }

    onSubmit() {
        this.error = false;
        if (!this.credentials.password || !this.credentials.username) {
            this.error = true;
            return;
        }
        this.processing = true;
        this.auth.doLogin(this.credentials).then(
            () => {
                this.processing = false;
                if (UserStorage.getUser().permission === Permission.artist) {
                    this.router.navigate(['/kepeim']);
                }
                else if (UserStorage.getUser().permission === Permission.mentor) {
                    this.router.navigate(['/ertekeles']);
                }
                else {
                    this.router.navigate(['/vezerlopult']);
                }

                this.appComp.ngOnInit();
            },
            () => {
                this.processing = false;
                this.msgs.add({
                    severity: 'error',
                    summary: 'Helytelen belépési adatok!'
                });
            }
        );
    }

}

export interface ICredentials {
    username: string,
    password: string
}
