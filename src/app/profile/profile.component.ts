import {Component, OnInit} from '@angular/core';
import {environment} from '../../environments/environment';
import {Artist} from '../settings/settings.component';
import {UserStorage} from '../shared/services/UserStorage';
import {IUser} from '../shared/services/auth.service';
import {AuthService} from '../shared/services/auth.service';
import {ICredentials} from '../login/login.component';
import {MessageService} from 'primeng/api';
import {BaseService} from '../shared/services/BaseService';


@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
    getuser: IUser = <any>{};
    user: Artist = <any>{};
    emailDisplay: boolean = false;
    tmpEmail: string;
    newPassword: string;
    newPasswordRepeat: string;
    passwordDisplay: boolean = false;
    credentials: ICredentials = <any>{};
    private processing: boolean;
    public uploadURL: string = environment.backend + '/api/files/add';

    constructor(private msgs: MessageService, private baseService: BaseService, private auth: AuthService) {
    }

    ngOnInit() {
        this.getuser = <any>UserStorage.getUser();
        this.credentials.username = this.getuser.username;
        this.user.name = this.getuser.name;
        this.user.id = this.getuser.id;
        this.user.permission = this.getuser.permission;
        this.user.username = this.getuser.username;
        this.user.logo_file_path = this.getuser.logo_file_path;
    }

    public prepareUpload(event) {
        this.processing = true;
        let token = localStorage.getItem(AuthService.TOKEN_NAME);
        event.xhr.setRequestHeader('Authorization', 'Bearer ' + token);
        event.formData.append('uploader_id', '' + UserStorage.getUser().id);
    }

    fileError() {
        this.processing = false;

        this.msgs.add({
            severity: 'error',
            summary: 'Nem sikerült a fájl feltöltése!'
        });
    }

    fileUploaded(event) {

        let resp = JSON.parse(event.xhr.response);
        this.user.logo_file_path = resp[0]['file_path'];
        this.update();
    }

    onEmailSubmit() {
        if (!this.tmpEmail) {
            return;
        }
        this.user.email = this.tmpEmail;
        this.update();
        this.emailDisplay = false;
    }

    cancel() {
        this.emailDisplay = false;
        this.passwordDisplay = false;
    }

    showEmail() {
        this.tmpEmail = this.user.email;
        this.emailDisplay = true;
    }

    showPassword() {
        this.passwordDisplay = true;
        this.newPassword = '';
        this.newPasswordRepeat = '';
    }

    onPasswodSubmit() {

        this.auth.doLogin(this.credentials).then(
            () => {
                if (!this.newPassword || !this.newPasswordRepeat) {
                    this.msgs.add({
                        severity: 'error',
                        summary: 'Minden mező kitöltése kötelező!'
                    });
                    return;
                }
                if (this.newPassword != this.newPasswordRepeat) {
                    this.msgs.add({
                        severity: 'error',
                        summary: 'Az új jelszavak nem egyeznek!'
                    });
                    return;
                }
                this.user.password = this.newPassword;
                this.update();

            },
            () => {
                this.msgs.add({
                    severity: 'error',
                    summary: 'Hibás jelszó!'
                });
            }
        );
    }

    update() {
        this.baseService.update('users', this.user.id, this.user).subscribe(
            (resp) => {
                this.user = resp;
                this.msgs.add({
                    severity: 'success',
                    summary: 'Sikeres módosítás'
                });
                this.passwordDisplay = false;
            },
            () => {
                this.msgs.add({
                    severity: 'error',
                    summary: 'Sikertelen módosítás'
                });
            });
        this.passwordDisplay = false;
    }


}
