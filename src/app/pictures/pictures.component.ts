import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import * as moment from 'moment';
import {ConfirmationService, MessageService} from 'primeng/api';
import {Category} from '../settings/settings.component';
import {BaseService} from '../shared/services/BaseService';
import {CategoryStorage} from '../shared/services/CategoryStorage';
import {FileNumStorage} from '../shared/services/FileNumStorage';
import {UserStorage} from '../shared/services/UserStorage';

@Component({
    selector: 'app-pictures',
    templateUrl: './pictures.component.html',
    styleUrls: ['./pictures.component.css']
})
export class PicturesComponent implements OnInit {
    id: string;
    pictures: sorce[];
    public categories: Category[] = [];
    public filterObj: IFilterObj = {};
    public myImagesByCat = [];
    public myPicNum: number = 0;
    private timeout = null;
    public maxPicNum: number = 10;

    constructor(
        private base: BaseService,
        private msgs: MessageService,
        private confirm: ConfirmationService,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.categories = CategoryStorage.getCats();

        this.filterObj = <any>{
            artist_id: UserStorage.getUser().id,
            groupBy: 'category',
            take: 3,
            showRefusedImages: 1,
            showFreshImages: 1,
            withRevisions : 1,
        };

        this.fetchMyImages();
        this.startTimer();
    }

    public startTimer() {
        clearTimeout(this.timeout);
        let now = moment();
        this.timeout = setTimeout(() => {
            this.categories.forEach(
                e => {
                    if (!!this.myImagesByCat[e.name]) {
                        this.myImagesByCat[e.name] = this.myImagesByCat[e.name].map(
                            (p) => {
                                let goalTime = moment(p.created_at).add(3, 'minutes');
                                if (now.valueOf() > goalTime.valueOf()) {
                                    p.publicTime = null;
                                }
                                else {
                                    let duration = moment.duration(goalTime.diff(now));
                                    p.publicTime = moment.utc(duration.asMilliseconds()).format('HH:mm:ss');
                                }
                                return p;
                            }
                        );
                    }
                    this.startTimer();
                }
            );
        }, 1000);
    }

    private fetchMyImages() {
        this.base.post('pictures/filterimages', this.filterObj).subscribe(
            (resp) => {
                this.myImagesByCat = resp;
                FileNumStorage.setFileNumsByCategory(this.calcuateImageNum());
                this.myPicNum = FileNumStorage.getSum();
            },
            () => {
                this.msgs.add({
                    severity: 'error',
                    summary: 'Nem sikerükt a fotók betöltése!'
                });
            }
        );
    }

    private calcuateImageNum(): any {
        let data = {};
        this.categories.forEach(
            e => {
                if (!!this.myImagesByCat[e.name]) {
                    data[e.name] = this.myImagesByCat[e.name].length;
                }
            }
        );
        return data;
    }

    deletePicture(id: number, category: string) {
        this.base.delete('pictures', id).subscribe(
            () => {
                this.myImagesByCat[category] = this.myImagesByCat[category].filter(e => e.id !== id);
                this.msgs.add({
                    severity: 'success',
                    summary: 'Sikeres törlés!'
                });
                this.myPicNum--;
                localStorage.setItem('uploaded_num', ''+this.myPicNum);
            },
            () => {
                this.msgs.add({
                    severity: 'error',
                    summary: 'Nem sikerült a törlés!'
                });
            });
    }


    updatePicture(id: number) {
        this.router.navigate(['/feltolt', id]);
    };

    confirmDelete(id: number, category: string) {
        this.confirm.confirm({
            message: 'Biztosan törölni szeretné?',
            accept: () => {
                this.deletePicture(id, category);
            },
            reject: () => {
            }
        });
    }

}

export interface sorce {
    url: string,
    score: number,
    viewed: number,
    title?: string,
    category: string,
    description?: string,
    artist: string
    tmpscore?: number;
}

export interface IFilterObj {
    orderBy?: string;
    category_id?: number;
    artist_id?: number;
    orderDirection?: string;
    groupBy?: string
    take?: string
}