import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {NgxImageEditorModule} from 'ngx-image-editor';
import {MessageService} from 'primeng/api';
import {ButtonModule} from 'primeng/button';
import {InputTextareaModule, MenubarModule, OverlayPanelModule} from 'primeng/primeng';
import {ToastModule} from 'primeng/toast';
import {environment} from '../environments/environment';
import {JwtModule} from '@auth0/angular-jwt';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component'
import {InputTextModule} from 'primeng/inputtext';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { PicturesComponent } from './pictures/pictures.component';
import {AdminGuard} from './shared/services/admin.guard';
import {ArtistGuard} from './shared/services/artist.guard';
import {AuthGuard} from './shared/services/auth.guard';
import {AuthService} from './shared/services/auth.service';
import {BaseService} from './shared/services/BaseService';
import {MentorGuard} from './shared/services/mentor.guard';
import { UploadComponent } from './upload/upload.component';
import { ProfileComponent } from './profile/profile.component';
import { FileUploadModule} from 'primeng/fileupload';
import {DropdownModule} from 'primeng/dropdown';
import { PublicDashboardComponent } from './public-dashboard/public-dashboard.component';
import { SettingsComponent } from './settings/settings.component';
import {DialogModule} from 'primeng/dialog';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService} from 'primeng/api';
import {CalendarModule} from 'primeng/calendar';
import { PhotoEditorComponent } from './photo-editor/photo-editor.component';
import { CategoryComponent } from './category/category.component';
import { ImageLinkPipe } from './shared/services/image-link.pipe';
import { ScoringComponent } from './scoring/scoring.component';
import {RatingModule} from 'primeng/rating';
import { ImageModalComponent } from './image-modal/image-modal.component';
import {ChartModule} from 'primeng/chart';
import { StatisticComponent } from './statistic/statistic.component';
import { RatingComponent } from './rating/rating.component';
import { AdvertismentComponent } from './advertisment/advertisment.component';
import { AdminPicturesComponent } from './admin-pictures/admin-pictures.component';

export function tokenGetter() {
    return localStorage.getItem(AuthService.TOKEN_NAME);
}


@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        PicturesComponent,
        UploadComponent,
        ProfileComponent,
        PublicDashboardComponent,
        SettingsComponent,
        PhotoEditorComponent,
        CategoryComponent,
        ImageLinkPipe,
        ScoringComponent,
        ImageModalComponent,
        StatisticComponent,
        RatingComponent,
        AdvertismentComponent,
        AdminPicturesComponent

    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        HttpClientModule,
        ReactiveFormsModule,
        FlexLayoutModule,
        NgxImageEditorModule,
        //PrimeNG

        ToastModule,
        InputTextareaModule,
        InputTextModule,
        MenubarModule,
        ButtonModule,
        FileUploadModule,
        DropdownModule,
        DialogModule,
        ConfirmDialogModule,
        OverlayPanelModule,
        CalendarModule,
        RatingModule,
        ChartModule,

        JwtModule.forRoot({
            config: {
                tokenGetter: tokenGetter,
                blacklistedRoutes: [environment.backendDomain + AuthService.LOGIN_ROUTE],
                whitelistedDomains: [environment.backendDomain]
            }
        }),

        AppRoutingModule
    ],
    providers: [AuthService, AuthGuard, MessageService, ConfirmationService, AppComponent, BaseService, ArtistGuard, MentorGuard, AdminGuard],
    bootstrap: [AppComponent]

})
export class AppModule {}
