import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {MessageService} from 'primeng/api';
import {BaseService} from '../shared/services/BaseService';
import {ImageVisitsStorage} from '../shared/services/ImageVisitsStorage';
import {UserStorage} from '../shared/services/UserStorage';

@Component({
    selector: 'app-image-modal',
    templateUrl: './image-modal.component.html',
    styleUrls: ['./image-modal.component.css']
})
export class ImageModalComponent implements OnInit {

    public visble: boolean = false;

    @Input() img: any = {};


    @Output() visibleChange = new EventEmitter<any>();

    @Input()
    get visible() {
        return this.visble;
    }

    set visible(val) {
        if (val) {
            let ids = ImageVisitsStorage.getVisitedIDs();
            if (!ids || !ids.includes(this.img.id)) {
                this.doVisitRecord();
                ImageVisitsStorage.addId(this.img.id);
            }
        }
        this.visble = val;
        this.visibleChange.emit(this.visble)
    }

    get isAlreadyLiked() {
        let ids = ImageVisitsStorage.getLikedIDs();
        return !!ids.length && !!ids.includes(this.img.id);
    }


    public isLoggedIn: boolean = !!UserStorage.getUser();

    onHide() {
        this.visble = false;
    }

    constructor(
        private base: BaseService,
        private msgs: MessageService
    ) {
    }

    ngOnInit() {
    }

    likeImage() {
        if (!this.isAlreadyLiked) {
            this.sendLike();
            ImageVisitsStorage.addLikeId(this.img.id);
        }
    };

    private sendLike() {
        this.base.get('pictures/imageLiked/' + this.img.id).subscribe(
            () => {
                this.msgs.add({
                    severity: 'success',
                    summary: 'Sikeres likeolás'
                });
            });
    }

    private doVisitRecord() {
        this.base.get('pictures/imageVisited/' + this.img.id).subscribe();
    }

}
