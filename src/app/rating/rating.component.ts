import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import * as moment from 'moment';
import {ConfirmationService, MessageService} from 'primeng/api';
import {Category} from '../settings/settings.component';
import {BaseService} from '../shared/services/BaseService';
import {CategoryStorage} from '../shared/services/CategoryStorage';
import {FileNumStorage} from '../shared/services/FileNumStorage';
import {UserStorage} from '../shared/services/UserStorage';
import {sorce, IFilterObj} from '../pictures/pictures.component';


@Component({
    selector: 'app-rating',
    templateUrl: './rating.component.html',
    styleUrls: ['./rating.component.css']
})
export class RatingComponent implements OnInit {
    id: string;
    pictures: sorce[];
    public categories: Category[] = [];
    public filterObj: IFilterObj = {};
    public myImagesByCat = [];

    constructor(
        private base: BaseService,
        private msgs: MessageService,
        private confirm: ConfirmationService,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.categories = CategoryStorage.getCats();

        this.filterObj = <any>{
            groupBy: 'category',
            take: 10,
            withRevisions: 1,
            revisionOrder: 1,
            revisionsOfMentorId: UserStorage.getUser().id,
        };

        this.fetchMyImages();
    }

    private fetchMyImages() {
        this.base.post('pictures/filterimages', this.filterObj).subscribe(
            (resp) => {
                this.myImagesByCat = resp;
            },
            () => {
                this.msgs.add({
                    severity: 'error',
                    summary: 'Nem sikerükt a fotók betöltése!'
                });
            }
        );
    }
}
