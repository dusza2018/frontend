import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MessageService, SelectItem} from 'primeng/api';
import {IFilterObj} from '../pictures/pictures.component';
import {BaseService} from '../shared/services/BaseService';
import {CategoryStorage} from '../shared/services/CategoryStorage';
import {getAllDebugNodes} from '@angular/core/src/debug/debug_node';

@Component({
    selector: 'app-category',
    templateUrl: './category.component.html',
    styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

    public filterObj: IFilterObj = {};
    public pictures: any[] = [];
    public categories = CategoryStorage.getCats();
    public category:any  = {};
    public selectedUserId = null;
    public processing: boolean = false;
    public userOptions: SelectItem[] = [];
    public selectedPhoto = {};
    public modelVisible: boolean = false;
    public addDisplay: boolean = false;
    public add: Add = <any>{};

    constructor(
        private base: BaseService,
        private msgs: MessageService,
        private router: Router,
        private _route: ActivatedRoute
    ) {
        this._route.params.subscribe(params => {
            const catId = +params['id'];
            if (catId && catId > 0) {

                this.categories.forEach(e => {
                    if (e.id === catId) {
                        this.category = e;
                    }
                });

                this.filterObj = <any>{
                    category: catId
                };
                this.fetchImages();
            }
            else {
                this.msgs.add({
                    severity: 'error',
                    summary: 'A keresett kategória nem található!'
                });
            }
        });
    }

    ngOnInit() {
        this.fetchAllUser();
    }

    selectPhoto(pic) {
        if (new Date().valueOf() % 10 <= 1) {
            this.addDisplay = true;
            this.base.get('randomad').subscribe((resp) => {
                this.add = resp[0];

            });
        }
        this.selectedPhoto = pic;
        this.modelVisible = true;
    }

    //test

    public getFilteredData() {
        this.filterObj.artist_id = this.selectedUserId;
        this.fetchImages();
    }

    public fetchImages() {
        this.processing = true;
        this.base.post('pictures/filterimages', this.filterObj).subscribe((resp) => {
                this.pictures = resp;
            },
            () => {
                this.msgs.add({
                    severity: 'error',
                    summary: 'A képek betöltése nem sikerült!'
                });
            },
            () => this.processing = false)
    }

    public fetchAllUser() {
        this.base.get('users/allartist').subscribe((resp) => {
            this.userOptions = resp.map(e => {
                return {label: e.username, value: e.id};
            });
            this.userOptions.unshift({label: 'Mindenki', value: null})
        }, () => {
            this.msgs.add({
                severity: 'error',
                summary: 'A művészek betöltése nem sikerült!'
            });
        });
    }

    stepClick(adId: string) {
        this.base.get('stepclicks/'+adId).subscribe();
    }

}

interface Add {
    filepath: string,
    link: string,
    id?:any
}