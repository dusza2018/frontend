import {Component, OnInit} from '@angular/core';
import {ConfirmationService, MessageService, SelectItem} from 'primeng/api';
import {AppComponent} from '../app.component';
import {BaseService} from '../shared/services/BaseService';
import {CategoryStorage} from '../shared/services/CategoryStorage';
import * as moment from 'moment';


@Component({
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
    display: boolean = false;
    artistDisplay: boolean = false;
    category: Category = <any>{};
    categories: Category[] = [];
    artists: Artist[] = [];
    artist: Artist = <any>{};
    permissionOptions: SelectItem[] = [];
    error: boolean = false;
    public isEditing = false;


    public calendarLocale = {
        firstDayOfWeek: 1,
        dayNames: ['Vasárnap', 'Hétfő', 'Kedd', 'Szerda', 'Csütörtök', 'Péntek'],
        dayNamesShort: ['Vs', 'Hé', 'Ke', 'Sze', 'Cs', 'Pé', 'Szo'],
        dayNamesMin: ['V', 'H', 'K', 'Sz', 'CS', 'P', 'Sz'],
        monthNames: ['Január', 'Február', 'Március', 'Április', 'Május', 'Június', 'Július'
            , 'Augusztus', 'Szeptember', 'Október', 'November', 'December'],
        monthNamesShort: ['Jan', 'Febr', 'Márc', 'Ápr', 'Máj', 'Jún', 'Júl', 'Aug', 'Szept', 'Okt', 'Nov', 'Dec'],
        today: 'Ma',
        clear: 'Clear',
        rightDayNames: ['hétfő', 'kedd', 'szerda', 'csütörtök', 'péntek', 'szombat', 'vasárnap'],
    };

    constructor(
        private confirmationService: ConfirmationService,
        private baseService: BaseService,
        private appComp: AppComponent,
        private msgs: MessageService,
    ) {
    }


    changeCategory(c: Category) {
        this.isEditing = true;
        this.category = c;
        this.display = true;
    }

    changeArtist(art: Artist) {
        this.artist = art;
        this.artistDisplay = true;
        this.isEditing = true
    }

    ngOnInit() {
        this.categories = CategoryStorage.getCats().map(e => {
            e.dateForCalendar = new Date(<any>moment(e.deadline));
            return e;
        });
        this.baseService.get('users').subscribe(res => this.artists = res);
        this.permissionOptions = [
            {label: 'Fotós', value: 1},
            {label: 'Zsűri', value: 2}
        ];
        this.artist.password = 'asd';
    }

    showDialog() {
        this.display = true;
        this.isEditing = false;
        this.category = <any>{dateForCalendar: new Date()};
    }

    check() {
        this.error = false;
        if (!this.category.name || !this.category.deadline) {
            this.error = true;

            return;
        }
        this.onSubmit();
        this.display = false;
    }

    deleteArtist(id: number) {
        this.baseService.delete('users', id).subscribe(
            () => {
                this.artists = this.artists.filter(e => e.id !== id);
                this.appComp.ngOnInit();
                this.msgs.add({
                    severity: 'success',
                    summary: 'Sikeres törlés'
                });
            },
            () => {
                this.msgs.add({
                    severity: 'error',
                    summary: 'Sikertelen törlés'
                });
            }
        );
    }

    deleteCategory(id: number) {
        this.baseService.delete('categories', id).subscribe(
            () => {
                this.categories = this.categories.filter(e => e.id !== id);
                CategoryStorage.setCats(this.categories);
                this.appComp.ngOnInit();
                this.msgs.add({
                    severity: 'success',
                    summary: 'Sikeres törlés'
                });
            },
            () => {
                this.msgs.add({
                    severity: 'error',
                    summary: 'Sikertelen törlés'
                });
            }
        );

    }

    cancel() {
        this.category = <any>{};
        this.display = false;
        this.isEditing = false;
        this.artist = <any>{};
        this.artistDisplay = false;
    }

    a: any;

    onSubmit() {
        if (!this.category.name) {
            this.error = true;
            return;
        }
        if (this.isEditing) {
            this.updateCategory();
        }
        else {
            this.addCategory();
        }
    }

    public updateCategory() {
        if (!this.category.id) {return;}
        this.setDate();
        this.baseService.update('categories', this.category.id, this.category).subscribe(
            (resp) => {
                this.category = resp;
                this.categories = this.categories.filter(e => {
                    if (e.id === this.category.id) {
                        return this.category;
                    }
                    return e;
                });
                CategoryStorage.setCats(this.categories);
                this.appComp.ngOnInit();
                this.msgs.add({
                    severity: 'success',
                    summary: 'Sikeres módosítás'
                });
                this.display = false;
            },
            () => {
                this.msgs.add({
                    severity: 'error',
                    summary: 'Sikertelen módosítás'
                });
            });
    }

    private setDate() {
        this.category.deadline = moment(this.category.dateForCalendar).format('YYYY-MM-DD HH:mm:ss')
    }

    public addCategory() {
        this.setDate();
        this.baseService.post('categories', this.category).subscribe(
            (res) => {
                this.category = res;
                this.categories.push(this.category);
                CategoryStorage.setCats(this.categories);

                this.msgs.add({
                    severity: 'success',
                    summary: 'Sikeres hozzáadás'
                });
                this.display = false;
            }, () => {
                this.msgs.add({
                    severity: 'error',
                    summary: 'Sikertelen hozzáadás'
                });
            });
    }


    confirmDelete(id: number) {
        this.confirmationService.confirm({
            message: 'Biztosan törölni szeretné?',
            accept: () => {
                this.deleteCategory(id);
            },
            reject: () => {
            }
        });
    }

    confirmDeleteArtist(id: number) {
        this.confirmationService.confirm({
            message: 'Biztosan törölni szeretné?',
            accept: () => {
                this.deleteArtist(id);
            },
            reject: () => {
            }
        });
    }

    showAddArtist() {
        this.artistDisplay = true;
        this.artist = <any>{permission: 1};
    }

    onArtistSubmit() {
        if (!this.artist.name || !this.artist.username || !this.artist.permission) {
            this.error = true;
            return;
        }
        if (this.isEditing) {
            this.updateArtist();
        }
        else {
            this.addArtist();
        }
    }

    updateArtist() {
        this.baseService.update('users', this.artist.id, this.artist).subscribe(
            (resp) => {
                this.artist = resp;
                this.artists = this.artists.filter(e => {
                    if (e.id === this.artist.id) {
                        return this.artist;
                    }
                    return e;
                });
                this.appComp.ngOnInit();
                this.msgs.add({
                    severity: 'success',
                    summary: 'Sikeres módosítás'
                });
                this.artistDisplay = false;
            },
            () => {
                this.msgs.add({
                    severity: 'error',
                    summary: 'Sikertelen módosítás'
                });
            });
        this.isEditing = false;
    }

    addArtist() {
        this.artist.password = 'asd';
        this.baseService.post('users', this.artist).subscribe(
            (res) => {
                this.artist = res;
                this.artists.push(this.artist);
                this.msgs.add({
                    severity: 'success',
                    summary: 'Sikeres hozzáadás'
                });
                this.artistDisplay = false;
            }, () => {
                this.msgs.add({
                    severity: 'error',
                    summary: 'Sikertelen hozzáadás'
                });
                this.isEditing = false;
            });
    }

}


export interface Category {
    id?: number;
    name: string;
    description?: string;
    deadline: string;
    dateForCalendar?: Date
}

export interface Artist {
    created_at: string;
    logo_file_path: string;
    email?: string;
    id: number;
    name: string;
    permission: number;
    updated_at: string;
    username: string;
    password?: string;
}
