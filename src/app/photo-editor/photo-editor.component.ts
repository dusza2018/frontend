import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MessageService} from 'primeng/api';
import {environment} from '../../environments/environment';
import {BaseService} from '../shared/services/BaseService';
import {UserStorage} from '../shared/services/UserStorage';

@Component({
    selector: 'app-photo-editor',
    templateUrl: './photo-editor.component.html',
    styleUrls: ['./photo-editor.component.css']
})
export class PhotoEditorComponent implements OnInit {


    public isVisible: boolean = false;

    public config = null;

    @Output()
    visibleChange = new EventEmitter();

    @Input()
    get visible() {
        return this.isVisible;
    }

    set visible(val) {
        if (val) {
            this.config = {
                ImageName: 'Kép',
                ImageUrl: environment.backend + environment.imageLink + this.photoPath,
                ImageType: 'image/jpeg'
            };
            this.ngOnInit();
        }
        else {
            this.config = null;
        }
        this.isVisible = val;
        this.visibleChange.emit(this.isVisible);
    }


    @Input()
    photoPath: string = '';
    @Output()
    onFileUploaded = new EventEmitter();

    closeWin() {
        this.config = null;
        this.visible = false;
    }

    constructor(private base: BaseService, private msgs: MessageService) {
    }

    ngOnInit() {
    }

    public close() {
        this.isVisible = false;
    }

    public getEditedFile(file: File) {
        let fd = new FormData();
        fd.append('newFile[]', file, 'edited_file.jpg');
        fd.append('uploader_id', '' + UserStorage.getUser().id);
        this.base.post('files/add', fd).subscribe(
            (resp) => {
                resp[0].size = file.size;
                this.onFileUploaded.emit(resp[0]);
                this.isVisible = false;
                this.msgs.add({
                    severity: 'success',
                    summary: 'Sikeres vágás!'
                });
            },
            () => {
                this.msgs.add({
                    severity: 'error',
                    summary: 'A vágás nem sikerült!'
                });
            }
        );
    }

}
