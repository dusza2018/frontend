import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AdminPicturesComponent} from './admin-pictures/admin-pictures.component';
import {AdvertismentComponent} from './advertisment/advertisment.component';
import {CategoryComponent} from './category/category.component';
import {LoginComponent} from './login/login.component';
import { PicturesComponent } from './pictures/pictures.component';
import {PublicDashboardComponent} from './public-dashboard/public-dashboard.component';
import {AdminGuard} from './shared/services/admin.guard';
import {ArtistGuard} from './shared/services/artist.guard';
import {AuthGuard} from './shared/services/auth.guard';
import {MentorGuard} from './shared/services/mentor.guard';
import {StatisticComponent} from './statistic/statistic.component';
import { UploadComponent } from './upload/upload.component';
import { ProfileComponent } from './profile/profile.component';
import { SettingsComponent } from './settings/settings.component';
import { ScoringComponent } from './scoring/scoring.component';
import { RatingComponent } from './rating/rating.component';

const routes: Routes = [

    {path: "bejelentkezes", component: LoginComponent},
    {path: "kezdolap", component: PublicDashboardComponent},
    {path: "", component: PublicDashboardComponent},
    {path: "feltolt", component: UploadComponent, canActivate: [ArtistGuard]},
    {path: "feltolt/:id", component: UploadComponent, canActivate: [ArtistGuard]},
    {path: "kepeim", component: PicturesComponent, canActivate: [ArtistGuard]},
    {path: "profile", component: ProfileComponent, canActivate: [AuthGuard]},
    {path: "vezerlopult", component: SettingsComponent, canActivate: [AdminGuard]},
    {path: "kepsorrend", component: AdminPicturesComponent, canActivate: [AdminGuard]},
    {path: "hirdetesek", component: AdvertismentComponent, canActivate: [AdminGuard]},
    {path: "statisztika", component: StatisticComponent, canActivate: [AdminGuard]},
    {path: "kategoria/:id", component: CategoryComponent},
    {path: "ertekeles", component: ScoringComponent, canActivate: [MentorGuard]},
    {path: "ertekeleseim", component: RatingComponent, canActivate: [MentorGuard]}

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}