import {Component, OnInit} from '@angular/core';
import {MenuItem, MessageService} from 'primeng/api';
import {AuthService, IUser} from './shared/services/auth.service';
import {BaseService} from './shared/services/BaseService';
import {CategoryStorage} from './shared/services/CategoryStorage';
import {Permission} from './shared/services/Permission';
import {UserStorage} from './shared/services/UserStorage';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    items: MenuItem[];
    type:string=<any>{};
    changePermission:number=<any>{};
    nextPerm:string=<any>{};
    public user: IUser = <any>{};

    get isLoggedIn() {
        return this.auth.isLoggedIn();
    }

    constructor(public auth: AuthService,
                private db: BaseService,
                private msgs: MessageService
                ) {

    }

    private fetchCategories() {
        if (!CategoryStorage.getCats()) {
            this.db.get('categories').subscribe(
                (resp) => {
                    CategoryStorage.setCats(resp);
                },
                (err) => {
                    console.log(err);
                },
                () => {
                    this.setMenuItems();
                }
            );
        }
        else {
            this.setMenuItems();
        }
    }

    ngOnInit() {
        this.user = <any>UserStorage.getUser() || {};
        if(this.changePermission>0){
            this.user.permission=this.changePermission;
        }
        this.fetchCategories();
    }

    

    private setMenuItems() {
        this.items = [];
        let categoryItems = this.getCategoryMenuItems();
        if (!this.auth.isLoggedIn()) {
            this.items.push({label: 'Kezdőlap', routerLink: ['/kezdolap']});
            this.items.push(...categoryItems);
            this.items.push({label: 'Belépés', routerLink: ['/bejelentkezes']});
        }
        else {
            if (this.user.permission === Permission.artist) {
                this.items.push(...[
                    {label: 'Kezdőlap', routerLink: ['/kezdolap']},
                    {label: 'Új kép feltöltése', routerLink: ['/feltolt'], icon: 'fa fa-fw fa-bar-chart'},
                    {label: 'Képeim', routerLink: ['/kepeim'], icon: 'fa fa-fw fa-calendar'},
                    {
                        label: 'Kategóriák', items: categoryItems
                    }
                ]);
            }
            else if (this.user.permission === Permission.mentor) {
                this.items.push(...[
                    {label: 'Kezdőlap', routerLink: ['/kezdolap']},
                    {label: 'Értékelés', routerLink: ['/ertekeles']},
                    {label: 'Értékeléseim', routerLink: ['/ertekeleseim']},
                    {
                        label: 'Kategóriák', items: categoryItems
                    }
                ]);
            }
            else if (this.user.permission === Permission.admin) {
                this.items.push(...[
                    {label: 'Kezdőlap', routerLink: ['/kezdolap']},
                    {label: 'Statisztika', routerLink: ['/statisztika']},
                    {label: 'Értékelés', routerLink: ['/ertekeles']},
                    {label: 'Képsorrendek', routerLink: ['/kepsorrend']},
                    {label: 'Vezérlőpult', routerLink: ['/vezerlopult']},
                    {label: 'Hírdetések', routerLink: ['/hirdetesek']},
                    {
                        label: 'Kategóriák', items: categoryItems
                    }
                ]);
            }
            this.items.push({label: 'Profil', routerLink: ['/profile'], icon: 'fa fa-fw fa-calendar'});
        }
    }

    public getCategoryMenuItems(): MenuItem[] {
        let items = [];
        let cats = CategoryStorage.getCats();
        cats.forEach(e => {
            items.push({label: e.name, routerLink: ['/kategoria', e.id]})
        });

        return items;
    }

    public logout() {
        this.changePermission=null;
        this.auth.logOut();
        this.ngOnInit();
    }

    fromAdmin(perm:number){
        this.changePermission=perm;
        this.ngOnInit();
        this.msgs.add({
            severity: 'success',
            summary: 'Sikeres áttváltás'
        });
    }

}