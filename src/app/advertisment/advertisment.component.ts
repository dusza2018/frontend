import {Component, OnInit} from '@angular/core';
import {ConfirmationService, MessageService} from 'primeng/api';
import {environment} from '../../environments/environment';
import {AuthService} from '../shared/services/auth.service';
import {BaseService} from '../shared/services/BaseService';
import {UserStorage} from '../shared/services/UserStorage';

@Component({
    selector: 'app-advertisment',
    templateUrl: './advertisment.component.html',
    styleUrls: ['./advertisment.component.css']
})
export class AdvertismentComponent implements OnInit {

    public ads: any = [];
    public ad: IAd = <any>{};
    public uploadURL: string = environment.backend + '/api/files/add';
    private processing: boolean = false;
    public isEditing: boolean = false;
    public visible: boolean = false;

    constructor(
        private base: BaseService, private msgs: MessageService, private confSercice: ConfirmationService
    ) {
    }

    ngOnInit() {
        this.fetchAll();
    }

    public startUpdate(ad) {
        this.ad = ad;
        this.isEditing = true;
        this.visible = true;
    }

    public showDialog() {
        this.isEditing = false;
        this.ad = <any>{};
        this.visible = true;
    }

    public save() {
        if (!this.ad.filepath) {
            return;
        }
        if (this.isEditing) {
            this.updateAd();
        }
        else {
            this.saveAd();
        }
    }

    cancel() {
        this.visible = false;
    }

    public saveAd() {
        if (!this.ad.filepath) {
            return;
        }
        this.base.post('ads', this.ad).subscribe(
            (resp) => {
                this.ad = resp;
                this.ads.push(this.ad);
                this.msgs.add({
                    severity: 'success',
                    summary: 'Sikers mentés!'
                });
                this.visible = false;
            },
            () => {
                this.msgs.add({
                    severity: 'error',
                    summary: 'Nem sikerült a mentés!'
                });
            }
        );
    }

    public updateAd() {
        if (!this.ad.id || !this.ad.filepath) {
            return;
        }
        this.base.update('ads', this.ad.id, this.ad).subscribe(
            (resp) => {
                this.ad = resp;
                this.ads = this.ads.filter(e => {
                    if (e.id === this.ad.id) {
                        return this.ad;
                    }
                    return e;
                });
                this.visible = false;
                this.msgs.add({
                    severity: 'success',
                    summary: 'Sikers módosítás!'
                });
            },
            () => {
                this.msgs.add({
                    severity: 'error',
                    summary: 'Nem sikerült az adatok betöltése!'
                });
            }
        );
    }


    private fetchAll() {
        this.base.get('ads').subscribe((resp) => {
                this.ads = resp;
            },
            () => {
                this.msgs.add({
                    severity: 'error',
                    summary: 'Nem sikerült az adatok betöltése!'
                });
            });
    }

    public prepareUpload(event) {
        this.processing = true;
        let token = localStorage.getItem(AuthService.TOKEN_NAME);
        event.xhr.setRequestHeader('Authorization', 'Bearer ' + token);
        event.formData.append('uploader_id', '' + UserStorage.getUser().id);
    }

    fileError() {
        this.processing = false;

        this.msgs.add({
            severity: 'error',
            summary: 'Nem sikerült a fájl feltöltése!'
        });
    }

    fileUploaded(event) {
        let resp = JSON.parse(event.xhr.response);
        this.ad.filepath = resp[0]['file_path'];
    }

    confirmDelete(id: number) {
        this.confSercice.confirm({
            message: 'Biztosan törölni szeretné?',
            accept: () => {
                this.base.delete('ads', id).subscribe(
                    () => {
                        this.ads = this.ads.filter(e => e.id !== id);
                        this.msgs.add({
                            severity: 'success',
                            summary: 'Sikers törlés!'
                        });
                    },
                    () => {
                        this.msgs.add({
                            severity: 'error',
                            summary: 'Nem sikerült a törlés!'
                        });
                    }
                );
            },
            reject: () => {
            }
        });
    }


}

interface IAd {
    id: number,
    filepath: string,
    clicked: number,
    link: string
}
