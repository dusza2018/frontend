import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import * as moment from 'moment';
import {MessageService, SelectItem} from 'primeng/api';
import {environment} from '../../environments/environment';
import {AuthService} from '../shared/services/auth.service';
import {BaseService} from '../shared/services/BaseService';
import {CategoryStorage} from '../shared/services/CategoryStorage';
import {FileNumStorage} from '../shared/services/FileNumStorage';
import {UserStorage} from '../shared/services/UserStorage';

@Component({
    selector: 'app-upload',
    templateUrl: './upload.component.html',
    styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {
    image: Picture = <any>{};
    public processing = false;
    public processValue: number = 0;
    public invalidSize: boolean = false;
    categoryOptions: SelectItem[] = [];
    isEditing = false;
    public full: boolean = false;
    public editorVisible: boolean = false;
    public uploadURL: string = environment.backend + '/api/files/add';

    constructor(
        private base: BaseService,
        private msgs: MessageService,
        private router: Router,
        private _route: ActivatedRoute,
    ) {
    }

    ngOnInit() {
        const pictureId = +this._route.snapshot.params['id'];
        if (!!pictureId && pictureId > 0) {
            this.isEditing = true;
            this.base.get('pictures/' + pictureId).subscribe((resp) => {
                this.image = resp;
            }, () => {
                this.msgs.add({
                    severity: 'error',
                    summary: 'Nem sikerült a kép betöltése!'
                });
            })
        } else {
            let uploadedNum = FileNumStorage.getSum();
            if (uploadedNum === 10) {
                this.full = true;
            }
            this.isEditing = false;
            this.image = {filepath: null, category_id: null, artist_id: UserStorage.getUser().id};
        }
        let fileNumData = FileNumStorage.getFileNumsByCategory();
        this.categoryOptions = CategoryStorage.getCats().map(e => {
            let isDisabled = !!fileNumData[e.name] && fileNumData[e.name] >= 3;
            let message = !isDisabled ? '(határidő: ' + moment(e.deadline).format('MM.DD HH:mm') + ')' :
                ' - mind a 3 kép már fel van töltve!';

            return {
                label: e.name + message,
                value: e.id,
                disabled: isDisabled
            };
        });

        this.categoryOptions.unshift({label: 'Válassz kategóriát', value: null});
    }

    public prepareUpload(event: FileUploadEvent) {
        this.processing = true;
        this.processValue = 0;
        this.invalidSize = false;
        let token = localStorage.getItem(AuthService.TOKEN_NAME);
        event.xhr.setRequestHeader('Authorization', 'Bearer ' + token);
        event.formData.append('uploader_id', '' + UserStorage.getUser().id);
    }

    fileError() {
        this.processing = false;
        this.processValue = 0;
        this.invalidSize = false;

        this.msgs.add({
            severity: 'error',
            summary: 'Nem sikerült a fájl feltöltése!'
        });
    }

    changeUploadProgress(event) {
        this.processValue = event.progress;

    }

    fileUploaded(event) {
        this.processValue = 0;
        let resp = JSON.parse(event.xhr.response);
        this.image.filepath = resp[0]['file_path'];
        this.image.id = resp[0]['id'];
        this.image.fileSize = event.files[0].size / 1000000;
        if (this.image.fileSize > 4) {
            this.invalidSize = true;
        }
    }

    public deleteTmpImg() {
        this.processing = true;
        this.base.delete('files', this.image.id).subscribe(
            () => {
                this.image.filepath = null;
                this.image.fileSize = 0;
                this.invalidSize = false;
            },
            () => {
                this.image.filepath = null;
                this.msgs.add({
                    severity: 'error',
                    summary: 'Sikertelen törlés'
                });
            }, () => this.processing = false);
    }

    save() {
        if (!this.image.category_id || !this.image.filepath) {
            return;
        }
        if (!this.isEditing) {
            this.addPicture();
        }
        else {
            this.updatePicture();
        }

    }

    public updatePicture() {
        if (!this.image.id) {
            return;
        }
        this.base.update('pictures', this.image.id, this.image).subscribe(
            (resp) => {
                this.router.navigate(['/kepeim']);
                this.msgs.add({
                    severity: 'success',
                    summary: 'Sikeres mentés'
                });
            },
            () => {
                this.msgs.add({
                    severity: 'error',
                    summary: 'Nem sikerült a mentés'
                });
            }
        );
    }

    public addPicture() {
        this.base.post('pictures', this.image).subscribe(
            (resp) => {
                this.router.navigate(['/kepeim']);
                this.msgs.add({
                    severity: 'success',
                    summary: 'Sikeres mentés'
                });
            },
            () => {
                this.msgs.add({
                    severity: 'error',
                    summary: 'Nem sikerült a mentés'
                });
            }
        );
    }

    showEditor() {
        this.editorVisible = true;
    }

    cancel() {
        this.router.navigate(['/kepeim']);
    }

    public uploadedFile(event) {
       this.image.filepath = event.file_path;
       this.image.fileSize = event.fileSize / 1000000;
    }


}

interface FileUploadEvent {
    xhr: XMLHttpRequest,
    formData: FormData;
}


interface Picture {
    id?: number;
    filepath: string;
    title?: string;
    description?: string;
    category_id: number;
    artist_id: number;
    fileSize?: number;
}